// change once you have total number of tweets
const NUMBER_OF_TWEETS = 2322;
const TWEETS_TO_SHOW = 4;
const START_TIME = new Date(Date.now());

// slider settings
const slider = $('.slides');
const sliderTweets = $('.slides img');
const sliderResetsAfter = 2;
const sliderConfig = {
	arrows : false,
	draggable: false,
	speed: 1600,
	cssEase: 'cubic-bezier(0.375, 0.005, 0.210, 1.005)'
};

// player settings
var playCounter = 0;
var playFirstTime = true;
const playCues = {
	INTRO   : {start: 0, end: 5.83},
	WIPE1   : {start: 4.03, end: 6.63},
	TWEET1  : {start: 5.83, end: 13.17},
	TWEET2  : {start: 11.83, end: 19.13},
	TWEET3  : {start: 17.8, end: 25.13},
	TWEET4  : {start: 23.8, end: 30.6},
	WIPE2   : {start: 30.83, end: 32.59}
};
const player = flowplayer("#fp-background", {
	cuepoints: [
		playCues.INTRO.start,
		playCues.TWEET1.start,
		playCues.TWEET2.start,
		playCues.TWEET3.start,
		playCues.TWEET4.start,
		playCues.TWEET4.end,
		playCues.WIPE2.end
	],
	background: { mask: "transparent"},
	clip: { sources: [{ type: "video/mp4", src:  "./videos/_Carnival-July_4th-No_Out-2.mp4" }] }
});

function handleCue(cuepoint) {
	var sliderWillReset = !(playCounter % sliderResetsAfter);

	function handleTweet(index) {
		console.group(index, 'TWEET'+ index, location.href + '/' + $(sliderTweets[index-1]).attr("src"));
		slider.slick('slickNext');
		console.groupEnd();
	}

	function timer(startTime) {
		var t = new Date(Date.now()).getTime() - startTime.getTime();
		return Math.floor(t / 3600 / 1000) + ":" + Math.floor(t / 60 / 1000) + ":" + Math.floor(t / 1000);
	}

	switch (cuepoint.index) {
		case 0: console.group("ThankYouBurger " + playCounter); console.group(cuepoint.index,'INTRO');
			console.log("started at:", new Date(Date.now()).toLocaleTimeString(), "has been runnig for:", timer(START_TIME));
			console.log("playFirstTime:", playFirstTime);
			console.log("sliderResetsAfter:", sliderResetsAfter);
			sliderWillReset ? console.warn("Slider will reset after this run") : false;
			console.groupEnd();

			cuepoint.index === 0 && playFirstTime ? playFirstTime = false : slider.slick('slickNext'); // if first time dont go to next slide
			break;

		case 1: case 2: case 3: case 4:
		handleTweet(cuepoint.index); break;

		case 5: console.group(cuepoint.index, 'outro begins'); slider.slick('slickNext'); console.groupEnd(); break;

		case 6: console.group(cuepoint.index, 'outro ends');
			if (sliderWillReset) {
				console.warn("resetting slider"); slider.slick('unslick'); initSlider(sliderConfig);
			} else {
				slider.slick('slickNext');
			}

			setImages(sliderTweets); // reset images on end of outro

			playCounter++;
			console.groupEnd();

			console.groupEnd(); //end of thankYou Burger Group
			console.log('\n'); break;
	}
}

function randomizeImages(totalNumberOfImages, numberOfImagesToShow) {
	var previousSet = currentSet = new Array(numberOfImagesToShow);

	return randomSet();

	function randomSet() {
		!!previousSet[0]? currentSet.slice() : previousSet;

		for(var i = 0; i < numberOfImagesToShow; i++) {
			var newNumber = getNextRandom();

			while(previousSet.includes(newNumber) || currentSet.includes(newNumber)){
				newNumber = getNextRandom();
			}

			currentSet[i] = newNumber;
			previousSet = currentSet;
		}
		return currentSet;
	}

	function getNextRandom() {
		return Math.floor((Math.random() * totalNumberOfImages) + 1);
	}
}

function setImages(placeholder) {
	var random = randomizeImages(NUMBER_OF_TWEETS, TWEETS_TO_SHOW);
	random.forEach(function(imageNumber, index) {
		$(placeholder[index]).attr("src", "images/" + imageNumber + ".png");
	});
}

function initSlider(config) { playFirstTime = true; slider.slick(config) }

function initPlayer(player) { player.on("cuepoint", function(e, api, cuepoint) { handleCue(cuepoint) }) }

(function(){
	setImages(sliderTweets);
	initPlayer(player);
	initSlider(sliderConfig);
})();
